﻿namespace Actor
{
	using UnityEngine;
	
	//[RequireComponent( typeof( BaseActorBehaviour ) )]
	public abstract class BaseActionBehaviour : MonoBehaviour, IAction
	{
		[Header( "Base Settings:" )]
		
		[SerializeField]
		protected BaseActorBehaviour _baseActor;
		
		[SerializeField]
		protected bool _isActive = true;
		
		public virtual bool IsActive
		{
			set
			{
				_isActive = value;
				
				if ( value )
				{
					Refresh();
				}
			}
			
			get
			{
				return _isActive;
			}
		}
		
		public abstract string ActionName
		{
			get;
		}
		
		public virtual void Refresh()
		{
		}
		
		//--------------------------------------------------------
		#region [Loop]
		// called in update
		public virtual void Loop()
		{
		}
		
		// called in fixed update
		public virtual void FixedLoop()
		{
		}
		
		// called in late update
		public virtual void LateLoop()
		{
		}
		#endregion
		//--------------------------------------------------------
		
		// server or multiplayer function
		public virtual void DoCommand( int command )
		{
		}
	}
}
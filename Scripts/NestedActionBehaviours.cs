﻿namespace Actor
{
	using UnityEngine;
	using System.Collections.Generic;
	
	[ExecuteInEditMode]
	public class NestedActionBehaviours : MonoBehaviour
	{
		[SerializeField]
		private BaseActionBehaviour[] _actions = null;
		public IAction[] Actions
		{
			get
			{
				return _actions;
			}
		}
		
		[SerializeField]
		private bool _isSorted = false;
		
		public bool IsSorted
		{
			get
			{
				return _isSorted;
			}
		}
		
		[SerializeField]
		private string[] _sortActions = null;
		
		public string[] SortedActions
		{
			get
			{
				return _sortActions;
			}
		}
		
		private Dictionary<string, IAction> _actionsByName;
		public Dictionary<string, IAction> ActionsByName
		{
			get
			{
				return _actionsByName;
			}
		}
		
		private void Awake()
		{
			_actions = GetComponentsInChildren<BaseActionBehaviour>();
			
			if ( _actionsByName == null )
			{
				_actionsByName = new Dictionary<string, IAction>( 10 );
			}
			
			_actionsByName.Clear();
			
			for ( int i = 0; i < _actions.Length; ++i )
			{
				_actionsByName[ _actions[i].ActionName ] = _actions[i];
			}
			
#if VERBOSE
			Debug.Log( "Count: " + _actionsByName.Count );
#endif
		}
		
		public void Refresh()
		{
			for ( int i = 0; i < _actions.Length; ++i )
			{
				string actionName = _actions[i].ActionName;
				_actionsByName[ actionName ].Refresh();
			}
		}
	}
}

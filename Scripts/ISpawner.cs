﻿using UnityEngine;

namespace Actor
{
	public interface ISpawner
	{
		string Category
		{
			get;
		}
		
		void Shrink();
		IActor Spawn( Vector3 position, Quaternion rotation );
	}
}
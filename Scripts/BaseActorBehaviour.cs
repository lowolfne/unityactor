﻿#define VERBOSE

namespace Actor
{
	using UnityEngine;
	
	[RequireComponent( typeof( NestedActionBehaviours ) )]
	public abstract class BaseActorBehaviour : MonoBehaviour, IActor
	{
		[Header( "Base Settings:" )]
		
		[SerializeField]
		protected bool _isPlayer = false;
		public bool IsPlayer
		{
			get
			{
				return _isPlayer;
			}
		}
		
		[SerializeField]
		protected bool _isActive = true;
		
		public virtual bool IsActive
		{
			set
			{
				gameObject.SetActive( value );
				_isActive = value;
				
				if ( value && _nestedAction != null )
				{
					_nestedAction.Refresh();
				}
			}
			
			get
			{
				return _isActive;
			}
		}
		
		[SerializeField]
		private NestedActionBehaviours _nestedAction = null;
		public NestedActionBehaviours NestedAction
		{
			get
			{
				return _nestedAction;
			}
		}
		
		[SerializeField]
		protected int _busyFlag = 0;
		
		[SerializeField]
		private ActorSpeedLevel[] _actorSpeed = null;
		
		[SerializeField]
		protected int _speedLevel = 0;
		public int SpeedLevel
		{
			get
			{
				return _speedLevel;
			}
			set
			{
				if ( _actorSpeed == null || _actorSpeed.Length == 0 )
				{
#if VERBOSE
					Debug.LogError( "[BaseActorBehavior] Please set the _actorSpeed." );
#endif
					return;
				}
				
				if ( value >= _actorSpeed.Length )
				{
#if VERBOSE
					Debug.LogError( "[BaseActorBehavior] Actor level is invalid." );
#endif
					return;
				}
				
				_speedLevel = value;
				_speed = _actorSpeed[ _speedLevel ].Speed;
				
				if ( _animator != null )
				{
					_animator.speed = _actorSpeed[ _speedLevel ].AnimatorSpeed;
				}
			}
		}
		
		private float _speed;
		public float Speed
		{
			get
			{
				return _speed;
			}
		}
		
		[SerializeField]
		protected Animator _animator;
		public Animator Animator
		{
			get
			{
				return _animator;
			}
		}
		
		[SerializeField]
		protected Rigidbody _rigidbody;
		public Rigidbody Rigidbody
		{
			get
			{
				return _rigidbody;
			}
		}
		
		[SerializeField]
		protected Rigidbody2D _rigidbody2D;
		public Rigidbody2D Rigidbody2D
		{
			get
			{
				return _rigidbody2D;
			}
		}
		
		public virtual int Id
		{
			get
			{
				return gameObject.GetInstanceID();
			}
		}
		
		public abstract string ActorName
		{
			get;
		}
		
		public GameObject GameObject
		{
			get
			{
				return gameObject;
			}
		}
		
		//--------------------------------------------------------
		#region [Unity Override Methods]
		protected virtual void Start()
		{
			_isActive = gameObject.activeSelf;
			SpeedLevel = 0;
			
			ActorEvents.TriggerActorInitialise( this );
		}
		#endregion
		//--------------------------------------------------------
		
		//--------------------------------------------------------
		#region [Actions]
		public virtual void SetAllActionsActive( bool condition )
		{
			if ( _nestedAction == null )
			{
				return;
			}
			
			for ( int i = 0; i < _nestedAction.Actions.Length; ++i )
			{
				IAction action = _nestedAction.Actions[i];
				action.IsActive = condition;
			}
		}
		
		public virtual void TriggerAction( int id, int command, object parameter )
		{
			// overridable
		}
		#endregion
		//--------------------------------------------------------
		
		//--------------------------------------------------------
		#region [Busy Flags]
		public void SetBusyFlag( int flag )
		{
			_busyFlag |= ( flag );
		}
		
		public void ClearBusyFlag( int flag )
		{
			_busyFlag &= ~( flag );
		}
		
		public void ClearAllBusyFlag()
		{
			_busyFlag = 0;
		}
		
		public bool IsBusy( int flag )
		{
			return ( _busyFlag & ( flag ) ) == flag;
		}
		#endregion
		//--------------------------------------------------------
		
		//--------------------------------------------------------
		#region [Main Loops]
		public virtual void Loop()
		{
			IAction[] _actions = _nestedAction.Actions;
			bool isSorted = _nestedAction.IsSorted && _nestedAction.SortedActions != null;
			
			if ( isSorted )
			{
				string[] sortedActions = _nestedAction.SortedActions;
				
				for ( int i = 0; i < sortedActions.Length; ++i )
				{
					IAction currentAction = _nestedAction.ActionsByName[sortedActions[i]];
					
					if ( !currentAction.IsActive )
					{
						continue;
					}
					
					currentAction.Loop();
				}
			}
			else
			{
				for ( int i = 0; i < _actions.Length; ++i )
				{
					if ( !_actions[i].IsActive )
					{
						continue;
					}
					
					_actions[i].Loop();
				}
			}
		}
		
		public virtual void FixedLoop()
		{
			IAction[] _actions = _nestedAction.Actions;
			bool isSorted = _nestedAction.IsSorted && _nestedAction.SortedActions != null;
			
			if ( isSorted )
			{
				string[] sortedActions = _nestedAction.SortedActions;
				
				for ( int i = 0; i < sortedActions.Length; ++i )
				{
					IAction currentAction = _nestedAction.ActionsByName[sortedActions[i]];
					
					if ( !currentAction.IsActive )
					{
						continue;
					}
					
					currentAction.FixedLoop();
				}
			}
			else
			{
				for ( int i = 0; i < _actions.Length; ++i )
				{
					if ( !_actions[i].IsActive )
					{
						continue;
					}
					
					_actions[i].FixedLoop();
				}
			}
		}
		
		public virtual void LateLoop()
		{
			IAction[] _actions = _nestedAction.Actions;
			bool isSorted = _nestedAction.IsSorted && _nestedAction.SortedActions != null;
			
			if ( isSorted )
			{
				string[] sortedActions = _nestedAction.SortedActions;
				
				for ( int i = 0; i < sortedActions.Length; ++i )
				{
					IAction currentAction = _nestedAction.ActionsByName[sortedActions[i]];
					
					if ( !currentAction.IsActive )
					{
						continue;
					}
					
					currentAction.LateLoop();
				}
			}
			else
			{
				for ( int i = 0; i < _actions.Length; ++i )
				{
					if ( !_actions[i].IsActive )
					{
						continue;
					}
					
					_actions[i].LateLoop();
				}
			}
		}
		#endregion
		//--------------------------------------------------------
		
		//--------------------------------------------------------
		#region [Destroy Actor]
		public virtual void DestroyActor()
		{
			ActorEvents.TriggerDestroyActor( this, gameObject );
		}
		#endregion
		//--------------------------------------------------------
	}
}
﻿namespace Actor
{
	public interface IAction : IActiveable, ILoopable
	{
		string ActionName
		{
			get;
		}
		
		void Refresh();
	}
}
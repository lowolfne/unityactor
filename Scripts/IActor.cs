﻿using UnityEngine;

namespace Actor
{
	public interface IActor : ILoopable, IActiveable
	{
		int Id
		{
			get;
		}
		
		string ActorName
		{
			get;
		}
		
		bool IsPlayer
		{
			get;
		}
		
		NestedActionBehaviours NestedAction
		{
			get;
		}
		
		GameObject GameObject
		{
			get;
		}
		
		// todo have its own animator interface
		
		Animator Animator
		{
			get;
		}
		
		// todo have its own animator interface
		
		Rigidbody Rigidbody
		{
			get;
		}
		
		// todo have its own animator interface
		
		Rigidbody2D Rigidbody2D
		{
			get;
		}
		
		void SetAllActionsActive( bool condition );
		void DestroyActor();
	}
}
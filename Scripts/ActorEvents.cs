﻿namespace Actor
{
	using System;
	using UnityEngine;
	using System.Collections.Generic;
	
	public class ActorEvents
	{
		// initialise actor event.
		public static Action<IActor/*actor*/> _onActorInitialiseDelegate;
		public static event Action<IActor/*actor*/> OnActorInitialiseDelegateEvent
		{
			add
			{
				_onActorInitialiseDelegate -= value;
				_onActorInitialiseDelegate += value;
			}
			remove
			{
				_onActorInitialiseDelegate -= value;
			}
		}
		public static void TriggerActorInitialise( IActor actor )
		{
			if ( _onActorInitialiseDelegate != null )
			{
				_onActorInitialiseDelegate.Invoke( actor );
			}
		}
		// actor damage event.
		public static Action<GameObject/*actor*/, float/*damage*/, object/*parameter*/> _onActorDamageDelegate;
		public static event Action<GameObject/*actor*/, float/*damage*/, object/*parameter*/> OnActorDamageDelegateEvent
		{
			add
			{
				_onActorDamageDelegate -= value;
				_onActorDamageDelegate += value;
			}
			remove
			{
				_onActorDamageDelegate -= value;
			}
		}
		public static void TriggerActorDamage( GameObject actor, float damage, object parameter )
		{
			if ( _onActorDamageDelegate != null )
			{
				_onActorDamageDelegate.Invoke( actor, damage, parameter );
			}
		}
		// destroy actor event.
		public static Action<IActor/*actor*/, GameObject/*gameObject(actor)*/> _onDestroyActorDelegate;
		public static event Action<IActor/*actor*/, GameObject/*gameObject(actor)*/> OnDestroyActorDelegateEvent
		{
			add
			{
				_onDestroyActorDelegate -= value;
				_onDestroyActorDelegate += value;
			}
			remove
			{
				_onDestroyActorDelegate -= value;
			}
		}
		public static void TriggerDestroyActor( IActor actor, GameObject gameObject )
		{
			if ( _onDestroyActorDelegate != null )
			{
				_onDestroyActorDelegate.Invoke( actor, gameObject );
			}
		}
		// destroy actors event
		public static Action<List<IActor>/*actors*/> _onDestroyActorsDelegate;
		public static event Action<List<IActor>/*actors*/> OnDestroyActorsDelegateEvent
		{
			add
			{
				_onDestroyActorsDelegate -= value;
				_onDestroyActorsDelegate += value;
			}
			remove
			{
				_onDestroyActorsDelegate -= value;
			}
		}
		public static void TriggerDestroyActors( List<IActor> actors )
		{
			if ( _onDestroyActorsDelegate != null )
			{
				_onDestroyActorsDelegate.Invoke( actors );
			}
		}
		// actor spawned event.
		public static Action<ISpawner/*spawner*/> _onActorSpawnerSpawnedDelegate;
		public static event Action<ISpawner/*spawner*/> OnActorSpawnerSpawnedDelegateEvent
		{
			add
			{
				_onActorSpawnerSpawnedDelegate -= value;
				_onActorSpawnerSpawnedDelegate += value;
			}
			remove
			{
				_onActorSpawnerSpawnedDelegate -= value;
			}
		}
		public static void TriggerActorSpawnerSpawned( ISpawner spawner )
		{
			if ( _onActorSpawnerSpawnedDelegate != null )
			{
				_onActorSpawnerSpawnedDelegate.Invoke( spawner );
			}
		}
		
		// clear all events
		public static void Clear()
		{
			if ( _onActorDamageDelegate != null )
			{
				_onActorDamageDelegate = null;
			}
			
			if ( _onActorInitialiseDelegate != null )
			{
				_onActorInitialiseDelegate = null;
			}
			
			if ( _onDestroyActorDelegate != null )
			{
				_onDestroyActorDelegate = null;
			}
			
			if ( _onActorSpawnerSpawnedDelegate != null )
			{
				_onActorSpawnerSpawnedDelegate = null;
			}
			
			if ( _onDestroyActorsDelegate != null )
			{
				_onDestroyActorsDelegate = null;
			}
		}
	}
}

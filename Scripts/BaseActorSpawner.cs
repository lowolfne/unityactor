﻿using UnityEngine;
using System.Collections.Generic;

namespace Actor
{
	public class BaseActorSpawner : MonoBehaviour, ISpawner
	{
		[SerializeField]
		private string _category = null;
		
		public string Category
		{
			get
			{
				return _category;
			}
		}
		
		[SerializeField]
		private BaseActorBehaviour _prefab = null;
		
		public BaseActorBehaviour Prefab
		{
			get
			{
				return _prefab;
			}
		}
		
		[SerializeField]
		private Transform _parent = null;
		
		[SerializeField]
		private int _maxLimit = 20;
		
		[SerializeField]
		private int _minLimit = 1;
		
		private List<IActor> _removedActors = null;
		
		[SerializeField]
		private List<IActor> _actors = null;
		
		protected virtual void Start()
		{
			ActorEvents.TriggerActorSpawnerSpawned( this );
			
			_removedActors = new List<IActor>( 10 );
			_actors = new List<IActor>( 10 );
			
			for ( int i = 0; i < _minLimit; ++i )
			{
				InternalSpawn( false );
			}
		}
		
		public virtual IActor Spawn( Vector3 position, Quaternion rotation )
		{
			IActor actor = null;
			
			if ( _actors == null )
			{
				return actor;
			}
			
			for ( int i = 0; i < _actors.Count; ++i )
			{
				IActor temp = _actors[i];
				
				if ( !temp.IsActive )
				{
					actor = _actors[i];
					actor.IsActive = true;
					break;
				}
			}
			
			if ( actor == null )
			{
				actor = InternalSpawn( true );
			}
			
			actor.GameObject.transform.position = position;
			actor.GameObject.transform.rotation = rotation;
			
			return actor;
		}
		
		protected virtual IActor InternalSpawn( bool isActive )
		{
			Transform parent = _parent == null ? gameObject.transform : _parent;
			BaseActorBehaviour actor = Instantiate( _prefab, parent );
			actor.IsActive = isActive;
			
			if ( actor != null && _actors != null )
			{
				_actors.Add( actor );
			}
			
			return actor;
		}
		
		public virtual void Shrink()
		{
			if ( _actors == null || _actors.Count <= _minLimit )
			{
				return;
			}
			
			for ( int i = 0; i < _actors.Count; ++i )
			{
				IActor actor = _actors[i];
				
				if ( !actor.IsActive )
				{
					_removedActors.Add( actor );
				}
			}
			
			for ( int i = 0; i < _removedActors.Count; ++i )
			{
				IActor actor = _removedActors[i];
				_actors.Remove( actor );
			}
			
			ActorEvents.TriggerDestroyActors( _removedActors );
			
			_removedActors.Clear();
		}
	}
}

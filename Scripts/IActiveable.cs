﻿namespace Actor
{
	public interface IActiveable
	{
		bool IsActive
		{
			get;
			set;
		}
	}
}
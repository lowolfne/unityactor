﻿namespace Actor
{
	// unity execution Order event functions
	// https://docs.unity3d.com/Manual/ExecutionOrder.html
	
	using System.Collections.Generic;
	using UnityEngine;
	
	public class ActorHub : MonoBehaviour
	{
		[SerializeField]
		private int _actorCount = 0;
		
		private List<IActor> _actors;
		private List<IActor> _removedActors;
		private Dictionary<int, IActor> _actorsByName;
		private Dictionary<string, ISpawner> _actorSpawnerByCategory;
		
		private static ActorHub _instance;
		public static ActorHub Instance
		{
			get
			{
				return _instance;
			}
		}
		
		private void Awake()
		{
			if ( _instance == null )
			{
				_instance = this;
			}
			else if ( _instance != this )
			{
				Destroy( gameObject );
			}
			
			ActorEvents.OnActorInitialiseDelegateEvent += OnActorInitialise;
			ActorEvents.OnDestroyActorDelegateEvent += OnDestroyActor;
			ActorEvents.OnDestroyActorsDelegateEvent += OnDestroyActors;
			ActorEvents.OnActorSpawnerSpawnedDelegateEvent += OnActorSpawnerInitialise;
		}
		
		private void OnActorInitialise( IActor actor )
		{
			if ( _actorsByName == null )
			{
				_actorsByName = new Dictionary<int, IActor>( 10 );
			}
			
			if ( _removedActors == null )
			{
				_removedActors = new List<IActor>( 10 );
			}
			
			if ( _actors == null )
			{
				_actors = new List<IActor>( 10 );
			}
			
			if ( !_actorsByName.ContainsKey( actor.Id ) )
			{
				_actorsByName[ actor.Id ] = actor;
				_actors.Add( actor );
				
				_actorCount = _actors.Count;
			}
		}
		
		private void OnActorSpawnerInitialise( ISpawner spawner )
		{
			if ( _actorSpawnerByCategory == null )
			{
				_actorSpawnerByCategory = new Dictionary<string, ISpawner>( 10 );
			}
			
			if ( !_actorSpawnerByCategory.ContainsKey( spawner.Category ) )
			{
				_actorSpawnerByCategory[ spawner.Category ] = spawner;
			}
		}
		
		private void OnDestroyActor( IActor actor, GameObject gameObject )
		{
			if ( _removedActors == null )
			{
				_removedActors = new List<IActor>( 10 );
			}
			
			_removedActors.Add( actor );
			--_actorCount;
		}
		
		private void OnDestroyActors( List<IActor> actors )
		{
			if ( _removedActors == null )
			{
				_removedActors = new List<IActor>( 10 );
			}
			
			_removedActors.AddRange( actors );
			_actorCount -= actors.Count;
		}
		
		private void Update()
		{
			if ( _actors == null || _actors.Count <= 0 )
			{
				return;
			}
			
			for ( int i = 0; i < _actors.Count; ++i )
			{
				_actors[i].Loop();
			}
			
			if ( _actorSpawnerByCategory == null || _actorSpawnerByCategory.Count <= 0 )
			{
#if VERBOSE
				Debug.LogWarning( "[ActorHub] Spawner is empty!" );
#endif
				return;
			}
			
			foreach ( KeyValuePair<string, ISpawner> pair in _actorSpawnerByCategory )
			{
				pair.Value.Shrink();
			}
		}
		
		private void FixedUpdate()
		{
			if ( _actors == null || _actors.Count <= 0 )
			{
				return;
			}
			
			for ( int i = 0; i < _actors.Count; ++i )
			{
				_actors[i].FixedLoop();
			}
		}
		
		private void LateUpdate()
		{
			if ( _actors == null || _actors.Count <= 0 )
			{
				return;
			}
			
			for ( int i = 0; i < _actors.Count; ++i )
			{
				_actors[i].LateLoop();
			}
			
			// destroy actor
			if ( _removedActors == null || _removedActors.Count <= 0 )
			{
				return;
			}
			
			for ( int i = 0; i < _removedActors.Count; ++i )
			{
				IActor removedActor = _removedActors[i];
				
				_actors.Remove( removedActor );
				_actorsByName.Remove( removedActor.Id );
				Destroy( removedActor.GameObject );
			}
			
			_removedActors.Clear();
		}
		
		public void Clear()
		{
			_actorCount = 0;
			
			if ( _actorsByName != null )
			{
				_actorsByName.Clear();
			}
			
			if ( _actors != null )
			{
				_actors.Clear();
			}
			
			if ( _removedActors != null )
			{
				_removedActors.Clear();
			}
			
			if ( _actorSpawnerByCategory != null )
			{
				_actorSpawnerByCategory.Clear();
			}
		}
	}
}

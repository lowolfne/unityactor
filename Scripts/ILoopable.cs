﻿namespace Actor
{
	public interface ILoopable
	{
		void Loop();
		
		void FixedLoop();
		
		void LateLoop();
	}
}
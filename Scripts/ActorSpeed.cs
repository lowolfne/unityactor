﻿namespace Actor
{
	using System;
	
	[Serializable]
	public struct ActorSpeedLevel
	{
		public float Speed;
		public float AnimatorSpeed;
	}
}